// My Solution
// var count = 0;
// var buffer = require('fs').readFileSync(process.argv[2])
// var str = buffer.toString();

// for (i = 0; i < str.split('\n').length; ++i) {
//   count++;
// }
// console.log(count - 1);

// Better Solution
var fs = require('fs')
    
var contents = fs.readFileSync(process.argv[2])
var lines = contents.toString().split('\n').length - 1
console.log(lines)