var fs = require('fs')

function fileFilter(dir, extension, callback) {
  fs.readdir(dir, function(err, list) {
    if(err) {
      return callback(err);
    }
    var results = list.filter(function(element, i, array) {
      if(element.indexOf('.' + process.argv[3]) === -1) {
        return false;
      }
      else {
        return true;
      }
    });
  callback(null, results);
  });
}
module.exports = fileFilter;
